

; 1.43 
(define (repeated f n)
        (define (add-call res count)
                (if (= count 1)
                    res
                    (add-call (lambda (x) (res (f x))) (- count 1))))
        (add-call f n))
        

; 1.44
(define (average a b c) (/ (+ a b c) 3))

(define dx 0.0001)

(define (smooth f)
        (define (g x)
                (average (f (- x dx)) (f x) (f (+ x dx))))
        g)

(define (square x) (* x x))
(((repeated smooth 1) square) 11)


; 1.46
(define (iterative-improve good-enough? improve)
        (define (good guess)
                (if (good-enough? guess)
                    guess
                    (good (improve guess))))
        good)
