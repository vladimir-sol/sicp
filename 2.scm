(define (last-pair lst)
        (if (null? (cdr lst))
            (car lst)
            (last-pair (cdr lst))))

(define (remove-last lst)
        (if (null? (cdr lst))
            (list)
            (cons (car lst) (remove-last (cdr lst)))))

(define (reverse_ lst)
        (if (not (null? lst))
            (cons (last-pair lst) (reverse_ (remove-last lst)))
            (list)))

(define (my_filter func lst)
        (if (null? lst)
            (list)
            (if (func (car lst))
                (cons (car lst) (my_filter func (cdr lst)))
                (my_filter func (cdr lst)))))

(define (my_map func lst)
        (if (null? lst)
            (list)
            (cons (func (car lst)) (my_map func (cdr lst)))))

(define (same-parity . args)
        (let ((test (if (even? (car args))
                        even?
                        odd?)))
             (cons (car args) (my_filter test (cdr args)))))

(define (for-each func lst)
        (if (null? lst)
            #t
            ((lambda () (func (car lst)) (for-each func (cdr lst))))))


; 2.27
(define (deep-reverse lst)
          (if (not (null? lst))
            (if (list? (last-pair lst))
                (cons (deep-reverse (last-pair lst)) (deep-reverse (remove-last lst)))
                (cons (last-pair lst) (deep-reverse (remove-last lst))))
            (list)))


; 2.28
(define (fringe lst)
        (cond ((null? lst) (list))
              ((not (list? lst)) (list lst))
              (else (append (fringe (car lst)) (fringe (cdr lst))))))


; 2.29
(define (make-mobile left right)
        (list left right))

(define (make-branch length structure)
        (list length structure))

(define (left-branch mobile)
        (car mobile))

(define (right-branch mobile)
        (car (cdr mobile)))

(define (branch-length branch)
        (car branch))

(define (branch-structure branch)
        (car (cdr branch)))

(define (total-weight mobile)
        (if (not (list? mobile))  ; is int?
            mobile
            (+ (total-weight (branch-structure (left-branch mobile)))
               (total-weight (branch-structure (right-branch mobile))))))

(define (balanced? mobile)
        (let ((left-submobile (branch-structure (left-branch mobile)))
              (right-submobile (branch-structure (right-branch mobile))))
             ((and (= (* (branch-length (left-branch mobile)) 
                         (total-weight left-submobile))
                      (* (branch-length (right-branch mobile)) 
                         (total-weight right-submobile)))
                   (and (if (list? left-submobile)
                            (balanced? left-submobile)
                            #t)
                        (if (list? right-submobile)
                            (balanced? right-submobile)
                            #t))))))

(define (ismobile? x) #t)  ; implement and use instead of `list?`


; 2.30
(define (square-tree0 tree)
        (cond ((null? tree) (list))
              ((not (list? tree)) (* tree tree))
              (else (cons (square-tree (car tree)) (square-tree (cdr tree))))))

(define (square-tree tree)
        (map (lambda (x)
                     (cond ((null? x) (list))
                           ((not (list? x)) (* x x))
                           (else (square-tree x))))
             tree))


; 2.31
(define (tree-map func tree)
        (map (lambda (x)
                     (cond ((null? x) (list))
                           ((not (list? x)) (func x))
                           (else (tree-map func x))))
             tree))


; 2.32
(define (subsets s)
        (if (null? s)
            (list (list))
            (let ((rest (subsets (cdr s))))
                 (append rest (map (lambda (x) (cons (car s) x)) rest)))))
(define (accumulate op initial lst)
          (if (null? lst)
            initial
            (op (car lst)
                (accumulate op initial (cdr lst)))))

(define (enumerate-interval low high)
        (if (> low high)
            '()
            (cons low (enumerate-interval (+ low 1) high))))

(define (enumerate-tree tree) (fringe tree))

(define (sum-odd-squares tree)
        (accumulate +
                    0
                    (map (lambda (x) (* x x))
                         (filter odd?
                                 (enumerate-tree tree)))))


; 2.33
(define (map_ p sequence)
        (accumulate (lambda (x y) (cons (p x) y))
                    '()
                    sequence))

(define (append_ seq1 seq2)
        (accumulate cons
                    seq2
                    seq1))

(define (length_ sequence)
        (accumulate (lambda (x y) (+ 1 y)) 0 sequence))


; 2.35
(define (count-leaves tree)
        (accumulate +
                    0
                    (map (lambda (x) (if (list? x)
                                         (count-leaves x)
                                         1))
                         tree)))


; 2.36
(define (accumulate-n op init seqs)
        (if (null? (car seqs))
            (list)
            (cons (accumulate op init (map car seqs))
                  (accumulate-n op init (map cdr seqs)))))


; 2.37
(define (dot-product v w)
        (accumulate + 0 (map * v w)))

(define (matrix-*-vector m v)
        (map (lambda (row) (dot-product row v)) m))

(define (transpose mat)
        (accumulate-n cons (list) mat))

(define (matrix-*-matrix m n)
        (let ((cols (transpose n)))
             (map (lambda (mi) (matrix-*-vector cols mi)) m)))


; 2.39
(define (reverse1 sequence)
        (fold-right (lambda (el res) (append res (list el))) (list) sequence))

(define (reverse sequence)
        (fold-left (lambda (res el) (cons el res)) (list) sequence))


(define (flatmap func lst)
        (accumulate append (list) (map func lst)))


; 2.40
(define (unique-pairs n)
        (flatmap (lambda (x)
                     (map (lambda (y)
                                  (list x y))
                          (enumerate-interval 0 (- x 1))))
             (enumerate-interval 1 n)))


; 2.41
(define (unique-triples n)
        (flatmap (lambda (x)
                         (flatmap (lambda (y)
                                          (map (lambda (z)
                                                       (list x y z))
                                               (enumerate-interval 0 (- y 1))))
                                  (enumerate-interval 0 (- x 1))))
                 (enumerate-interval 1 n)))

(define (sum seq)
        (fold-left + 0 seq))

(define (triples-of s n)
        (filter (lambda (x) (= s (sum x)))
                (unique-triples n)))


; EVERYTHING BELOW WAS NEVER TESTED

; 2.44
(define (up-split painter n)
        (if (= n 0)
            painter
            (let ((smaller (up-split painter (- n 1))))
                 (below (beside smaller smaller)
                        painter))))


; 2.45
(define (split direction method)
        (define (inner painter n)
                (if (= n 0)
                    painter
                    (let ((smaller (inner painter (- n 1))))
                         (direction painter (method smaller smaller))))))


; 2.48
(define (make-segment start-vector end-vector)
        (list (make-vector (xcor-vect start-vector) 
                           (ycor-vect start-vector))
              (make-vector (xcor-vect end-vector)
                           (ycor-vect end-vector))))

(define (start-segment segment)
        (let ((start-vector (car segment)))
             (list (xcor-vect start-vector)
                   (ycor-vect start-vector))))


; 2.49
;a
(define frame-outline
        (lambda (frame)
                ((segments->painter (frame-borders! frame)) frame)))


; 2.50
(define (flip-horiz painter)
        (transform-painter painter
                           (make-vect 1.0 0.0)
                           (make-vect 0.0 0.0)
                           (make-vect 1.0 1.0)))


; 2.51
(define (below painter1 painter2)
        (lambda (frame)
                ((transform-painter painter2
                                    (make-vect 0.0 0.5)
                                    (make-vect 1.0 0.5)
                                    (make-vect 0.0 1.0)) frame)
                ((transform-painter painter1
                                    (make-vect 0.0 0.0)
                                    (make-vect 1.0 0.0)
                                    (make-vect 0.0 0.5)) frame)))


; 2.54
(define (myequal? lst1 lst2)
        (cond ((and (null? lst1) (null? lst2)) #t)
              ((or (null? lst1) (null? lst2)) #f)
              (else (and (eq? (car lst1) (car lst2))
                         (myequal? (cdr lst1) (cdr lst2))))))


; 2.59
(define (union-set set1 set2)
        (cond ((null? set1) set2)
              ((null? set2) set1)
              (else (if (in? (car set1) set2)
                        (union-set (cdr set1) set2)
                        (union-set (cdr set1)
                                   (cons (car set1) set2))))))

