
; 3.1
(define (make-accumulator sum)
        (lambda (x)
                (set! sum (+ sum x))
                sum))


; 3.2
(define (make-monitored f)
        (let ((counter 0))
             (lambda (x)
                     (if (eq? x 'how-many-calls?)
                         counter
                         (begin (set! counter (+ counter 1))
                                (f x))))))

; 3.3
(define (make-account balance password)
        (define (withdraw amount)
                (if (>= balance amount)
                    (begin (set! balance (- balance amount))
                           balance)
                    "Insufficient funds"))
        (define (deposit amount)
                (begin (set! balance (+ balance amount))
                       balance))

        (define (dispatch passwd operation)
                (cond ((not (eq? passwd password)) 'incorrect_password)
                      ((eq? operation 'withdraw) withdraw)
                      ((eq? operation 'deposit) deposit)
                      (else "Wrong operation")))
        dispatch)

; 3.3.4
; 3.3.5

; 3.4
(define (protected account)
        (let ((counter 0)
              (alert 7))
             (lambda (x y)
                     (let ((result (account x y)))
                          (begin (if (eq? result 'incorrect_password)
                                     (begin (set! counter (+ counter 1))
                                            (if (eq? counter alert)
                                                (begin (set! counter 0)
                                                       (call-the-cops)))))
                                 result)))))


; 3.6
(define (rand value)
        (let ((x random-init))
             (lambda (message)
                     (cond ((eq? message 'generate) (set! x (rand-update x))
                                                    x)
                           ((eq? message 'reset) (lambda (new-x)
                                                         (set! x new-x)))
                           (else "Wrong message")))))


; 3.7
(define (make-joint acc passwd new_passwd)
        (lambda (x y)
                (if (eq? x new_passwd)
                    (acc passwd y)
                    (acc x y))))


; 3.8
(define f
        (let ((first-call #t))
             (lambda (x)
                     (if first-call
                         (begin (set! first-call #f)
                                x)
                         0))))
                 

; 3.55
; cons -> cons-stream
; car -> stream-car
; map -> stream-map
(define (partial-sums S)
        (if (null? S)
            '()
            (cons (car S)
                  (map (lambda (x) (+ x (car S)))
                       (partial-sums (cdr S))))))


; 3.56
(define S (cons-stream 1
                       (merge (scale-stream S 2)
                              (merge (scale-stream S 3)
                                     (scale-stream S 5)))))


; 3.64
(define (stream-limit stream tolerance)
        (let ((first (car stream))
              (second (cadr stream)))
             (if (< (abs (- first second)) tolerance)
                 second
                 (stream-limit (cdr stream) tolerance))))
                



